﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Bank
    {
        private string bankName;
        private CardCenter center;
        private IList<Client> bankClients;

        public Bank(string bankName, CardCenter center)
        {
            this.bankName = bankName;
            this.center = center;
            bankClients = new List<Client>();
            
        }

        public void AddClient(Client client)
        {
            bankClients.Add(client);
        }
            
        public void RemoveClient(Client client)
        {
            if (bankClients.Contains(client))
                bankClients.Remove(client);
            else
                throw new Exception("Client not found on the list!");
        }

        public bool IsClientIncluded(Client client)
        {
            return bankClients.Contains(client);
        }

        public IList<Client> GetClients()
        {
            return bankClients;
        }

        public CardCenter GetCenter()
        {
            return center;
        }

        public string GetBankName()
        {
            return bankName;
        }

        public void Transfer(Card senderCard, Card receiverCard, float amount)
        {
            if (!center.Authorise(senderCard,amount))
                throw new NotEnoughMoneyException("Not enough money on the card!");
            else
            {
                receiverCard.AddMoney(amount);
                senderCard.RemoveMoney(amount);
                center.AddToArchive(new Transaction(senderCard, receiverCard, amount));
            }

        }
        public void PrintClients()
        {
            foreach (Client client in bankClients)
            {
                Console.WriteLine("#" + client.GetClientType() + "    " + client.ToString());
                client.PrintCards();
            }
        }

    





    }
}
