﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    abstract class Client
    {
        protected string clientName;
        protected IList<Card> clientCards;

        public Client(string name)
        {
            clientName = name;
            clientCards = new List<Card>();
        }
        public void Pay(Card senderCard, Card receiverCard, float amount)
        {
            if (clientCards.Contains(senderCard)) 
                senderCard.GetBank().Transfer(senderCard, receiverCard, amount);
            else
                throw new NotYourCardException("Using not your own card");
        }
        public IList<Card> GetCards()
        {
  
            return clientCards;
        }

        public void AddCard(Card card)
        {
            clientCards.Add(card);
        }
        public override string ToString()
        {
            return clientName;
        }
        public abstract string GetClientType();
        public new object GetType()
        {
            return typeof(Client);
        }
        public void PrintCards()
        {
            foreach(Card card in clientCards)
            {
                Console.WriteLine(">"+card.GetCardType() + "    " + card.GetNumber() + "    " + card.GetBalance() + "   " + card.GetOwner());
            }
        }
    }
}
