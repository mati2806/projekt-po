﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class DebitCard : Card
    {
        public DebitCard(Client client, Bank bank) : base(client, bank)
        {

        }

        public override string GetCardType()
        {
            return "DebitCard";
        }
    }
}
