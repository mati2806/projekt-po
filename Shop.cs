﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Shop : Client
    {
        public Shop(string name) : base(name)
        {

        }

        public override string GetClientType()
        {
            return "Shop";
        }
    }
}
