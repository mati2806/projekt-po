﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class NotYourCardException:Exception
    {
        private string msg;

        public NotYourCardException(string msg) : base(msg)
        {
            this.msg = msg;
        }
    }
}
