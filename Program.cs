﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Program
    {
        static void Main(string[] args)
        {
            CardCenter center = new CardCenter("Card Center");

            Bank santander = new Bank("Santander", center);
            Bank wbk = new Bank("WBK", center);
            Bank mbank = new Bank("MBank", center);

            center.AddBank(santander);
            center.AddBank(wbk);
            center.AddBank(mbank);

            Client john = new Shop("John");
            Client rachel = new Transport("Rachel");
            Client anthony= new Service("Anthony");
            Client james = new Shop("James");

            santander.AddClient(john);
            wbk.AddClient(rachel);
            wbk.AddClient(anthony);
            mbank.AddClient(rachel);
            mbank.AddClient(james);


            Card visa = new DebitCard(john, santander);
            Card visa1 = new ATMCard(john, santander);
            Card mastercard = new DebitCard(rachel, wbk);
            Card mastercard1 = new DebitCard(rachel, mbank);
            Card visa2 = new CreditCard(anthony, wbk);
            Card visa3 = new DebitCard(anthony, wbk);
            Card mastercard2 = new DebitCard(james, mbank);
            Card mastercard3 = new CreditCard(james, mbank);

            john.AddCard(visa);
            john.AddCard(visa1);
            rachel.AddCard(mastercard);
            rachel.AddCard(mastercard1);
            anthony.AddCard(visa2);
            anthony.AddCard(visa3);
            james.AddCard(mastercard2);
            james.AddCard(mastercard3);

            visa.AddMoney(800);
            visa1.AddMoney(1000);
            visa2.AddMoney(1000);
            visa3.AddMoney(1000);
            mastercard.AddMoney(1000);
            mastercard1.AddMoney(1000);
            mastercard2.AddMoney(1000);
            mastercard3.AddMoney(1000);

            try
            {
                john.Pay(visa, mastercard, 50);
                john.Pay(visa, mastercard1, 20);
                john.Pay(visa, visa2, 30);
                anthony.Pay(visa2, mastercard2, 52);
                anthony.Pay(visa3, mastercard1, 20);
                anthony.Pay(visa2, visa, 10); 
                rachel.Pay(mastercard, mastercard2, 40);
                rachel.Pay(mastercard1, mastercard, 70);
                rachel.Pay(mastercard1, visa1, 30);
                james.Pay(mastercard2, mastercard, 10);
                james.Pay(mastercard2, visa2, 60);
                james.Pay(mastercard3, visa1, 30);
            }
            catch(NotYourCardException)
            {
                Console.WriteLine("Not using your own card!");
            }
            catch(NotEnoughMoneyException)
            {
                Console.WriteLine("Not enough money on the card!");
            }

            center.PrintArchive();
            Console.WriteLine();

            Search search = new Search();

            try
            {
                search.addCriterium(anthony);
                search.addCriterium(mastercard2);
                foreach (Transaction t in search.RunOr(center))
                {
                    Console.WriteLine(t.ToString());
                }
            }
            catch(Exception)
            {
                Console.WriteLine("You can't use this type of criterium");
            }

            //center.PrintBanks();

            //center.SaveArchiveToFile();
            //center.LoadArchiveFromFile();



            Console.ReadKey();


        }
    }
}
