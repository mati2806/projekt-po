﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Search
    {
        private IList<object> criteriumList;
        private List<Transaction> returnList;

        public Search()
        {
            criteriumList = new List<object>();
            returnList = new List<Transaction>();
        }

        public void addCriterium(object o)
        {
            Type t = o.GetType();
            if (Equals(o.GetType().BaseType, typeof(Client)) || Equals(o.GetType(), typeof(Bank)) || Equals(o.GetType().BaseType, typeof(Card)) || Equals(o.GetType(), typeof(float)))
                criteriumList.Add(o);
            else
                throw new Exception("You can't use this type of criterium");
        }
        public void RemoveCriterium(object o)
        {
                criteriumList.Remove(o);
        }
        public void ClearCriterium()
        {
                criteriumList.Clear();
        }
        public void ShowCriteriumList()
        {
            foreach (object o in criteriumList)
            {
                Console.WriteLine(o.ToString());
            }
        }
        public List<Transaction> RunOr(CardCenter center)
        {
            returnList.Clear();
            for (int i = 0; i < criteriumList.Count; i++)
            {
                foreach (Transaction t in center.GetArchive())
                {
                    Type type = criteriumList.ElementAt(i).GetType();
                    var element = criteriumList.ElementAt(i);
                    if (!returnList.Contains(t))
                    {
                        if (Equals(type.BaseType, typeof(Client)))
                            if (t.GetRecieverCard().GetOwner() == element || t.GetSenderCard().GetOwner() == element)
                                returnList.Add(t);

                        if (Equals(type, typeof(Bank)))
                            if (t.GetRecieverCard().GetBank() == element || t.GetSenderCard().GetBank() == element)
                                returnList.Add(t);

                        if (Equals(type.BaseType, typeof(Card)))
                            if (t.GetRecieverCard() == element || t.GetSenderCard() == element)
                                returnList.Add(t);

                        if (Equals(type, typeof(float)))
                            if (t.GetAmount().Equals(element))
                                returnList.Add(t);
                    }
                }
            }
            return returnList;
        }

        public List<Transaction> RunAnd(CardCenter center)
        {
            returnList.Clear();
            returnList = center.GetArchive().ToList();
            int i = 0;
            while (returnList.Count() > 0 && i < criteriumList.Count())
            { 
                foreach (Transaction t in center.GetArchive())
                {
                    Type type = criteriumList.ElementAt(i).GetType();
                    var element = criteriumList.ElementAt(i);
                    if (Equals(type.BaseType, typeof(Client)))
                        if (t.GetRecieverCard().GetOwner() != element && t.GetSenderCard().GetOwner() != element)
                            returnList.Remove(t);
                    if (Equals(type, typeof(Bank)))
                        if (t.GetRecieverCard().GetBank() != element && t.GetSenderCard().GetBank() != element)
                            returnList.Remove(t);

                    if (Equals(type.BaseType, typeof(Card)))
                        if (t.GetRecieverCard() != element && t.GetSenderCard() != element)
                            returnList.Remove(t);

                    if (Equals(type, typeof(float)))
                        if (!(t.GetAmount().Equals(element)))
                            returnList.Remove(t);
                }
                i++;
            }
            return returnList;
        }
    }
}
