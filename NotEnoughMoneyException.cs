﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class NotEnoughMoneyException : Exception
    {
        private string msg;
        public NotEnoughMoneyException(string msg) : base(msg)
        {
            this.msg = msg;
        }
    }
}
