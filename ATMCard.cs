﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class ATMCard : Card
    {
        public ATMCard(Client client, Bank bank) : base(client, bank)
        {

        }

        public override string GetCardType()
        {
            return "ATMCard";
        }
    }
}
