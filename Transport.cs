﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Transport : Client
    {
        public Transport(string name) :base(name)
        {

        }

        public override string GetClientType()
        {
            return "Transport";
        }
    }
}
