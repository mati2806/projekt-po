﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Projekt_PO
{
    abstract class Card
    {
        protected static int id = 0; //used for generating next card numbers
        protected Bank cardBank;
        protected string cardNumber;
        protected Client cardOwner;
        protected float cardBalance;

        public Card(Client client, Bank bank) //Card object constructor @parm1 - Client object who is card owner @param2 - Bank object which created the card
        {
            cardNumber = (10000 + id).ToString();
            id++;
            cardBank = bank;
            cardOwner = client;
            cardBalance = 0;
        }
        public float GetBalance() //return balance float value
        {
            return cardBalance;
        }
        public Bank GetBank() //return Bank object which created card
        {
            return cardBank;
        }
        public string GetNumber() //return string value of card number
        {
            return cardNumber;
        }
        public Client GetOwner() //return Client object owner
        {
            return cardOwner;
        }
        public void AddMoney(float amount) //method for putting money on the card
        {
            cardBalance += amount;
        }
        public void RemoveMoney(float amount) //method for removing money from the card
        {
            cardBalance -= amount;
        }
        public abstract string GetCardType();


    }
}
