﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class Transaction
    {
        private static int globalID = 0;
        private int id;
        private Card senderCard;
        private Card receiverCard;
        private float amount;

        public Transaction(Card senderCard, Card receiverCard, float amount)
        {
            this.senderCard = senderCard;
            this.receiverCard = receiverCard;
            this.amount = amount;
            id = globalID;
            globalID++;
        }

        public int GetID()
        {
            return id;
        }

        public Card GetSenderCard()
        {
            return senderCard;
        }

        public Card GetRecieverCard()
        {
            return receiverCard;
        }

        public float GetAmount()
        {
            return amount;
        }

        public override string ToString()
        {
            return "ID: " + id.ToString() + " value: " + amount.ToString() + " from: " + senderCard.GetNumber() + "(" + senderCard.GetOwner().ToString() + ")" + " to: " + receiverCard.GetNumber() + "(" + receiverCard.GetOwner().ToString() + ")";
        }
    }
}
