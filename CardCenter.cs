﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_PO
{
    class CardCenter 
    {
        private string name;
        private IList<Bank> banks;
        private IList<Transaction> archive;

        public CardCenter(string name) //constructor
        {
            this.name = name;
            banks = new List<Bank>();
            archive = new List<Transaction>();
        }

        public void AddBank(Bank bank) //adds bank to list
        {
            banks.Add(bank);
        }

        public void RemoveBank(Bank bank) //removes bank from list
        {
            banks.Remove(bank);
        }

        public bool IsBankIncluded(Bank bank) //checks if bank is on the list
        {
            return banks.Contains(bank);
        }

        public IList<Bank> GetBanks() //returns list of banks
        {
            return banks;
        }

        public bool Authorise(Card card, float amount) 
        {
            if (card.GetBalance() < amount)
                return false;
            else
                return true;
        }

        public IList<Transaction> GetArchive() //returns list of transactions
        {
            return archive;
        }

        public void SaveArchiveToFile() //saves archive to file
        {
            Console.Write("Name your save file: ");
            string name = Console.ReadLine();
            string path = System.IO.Directory.GetCurrentDirectory() + @"\" + name;
            Console.WriteLine("Saved archive to: " + path);
            StreamWriter sw = new StreamWriter(path);

            foreach (Transaction transaction in archive)
            {
                sw.WriteLine("ID: " + transaction.GetID() + " Value: " + transaction.GetAmount() + " From: " + transaction.GetSenderCard().GetNumber() + "(" + transaction.GetSenderCard().GetOwner() + ")" + " To: " + transaction.GetRecieverCard().GetNumber() + "(" + transaction.GetRecieverCard().GetOwner() + ")");

            }
            sw.Close();        
        }

        public void LoadArchiveFromFile() //loads archive from file and prints it on the screen
        {
            Console.Write("Type name of the file to load: ");
            string name = Console.ReadLine();
            string path = System.IO.Directory.GetCurrentDirectory() + @"\" + name;
            Console.WriteLine("Loaded archive from: " + path);
            StreamReader sr = new StreamReader(path);

            Console.WriteLine("Loaaded from archive:");
            Console.WriteLine(sr.ReadToEnd());           
        
        }

        public void AddToArchive(Transaction transaction) //adds transaction to the archive
        {
            archive.Add(transaction);
        }

        public void PrintBanks() //prints list of banks
        {
            int i = 0;
            foreach (Bank bank in banks)
            {

                Console.WriteLine();
                Console.WriteLine(i + " " + bank.GetBankName());
                bank.PrintClients();
                i++;
              
            }
        }

        public void PrintArchive() //prints list of transactions
        {
     
            foreach (Transaction transaction in archive)
            {
                Console.WriteLine("ID: " + transaction.GetID() + " Value: " + transaction.GetAmount() + " From: " + transaction.GetSenderCard().GetNumber() + "("  +transaction.GetSenderCard().GetOwner() +")" + " To: " + transaction.GetRecieverCard().GetNumber() + "("+ transaction.GetRecieverCard().GetOwner() + ")");

            }
        }
    }
}
